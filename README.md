# GNUPLOT-GO library

This is a really simple library which generates gnuplot input files and calls 
gnuplot on those input files to generate plots.

Currently it is capable of generating:
* Scatter plots
* Map plots (pm3d map)

I plan to add some examples of its usage soon.