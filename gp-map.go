/*
Copyright (c) 2012, Michael Leimon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL LEIMON BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// --- `gp-map.go` - map plotting functions

package gnuplot

import (
	"fmt"
	"io/ioutil"
	"strings"
	"os/exec"
	"os"
)


func TESTmap(){
	// === Generate an input file and corresponding plot
	testVals := [][]float64{[]float64{1, 2, 3, 2, 1},
		[]float64{2, 3, 4, 3, 2},
		[]float64{3, 4, 5, 4, 3},
		[]float64{2, 3, 4, 3, 2},
		[]float64{1, 2, 3, 2, 1} }

	mapPlot := NewMap(testVals)
	mapPlot.FNAME = "TESTmapPlot"
	mapPlot.Set("title", "TESTmapPlot")
	mapPlot.SetFlag("saveinput")
	mapPlot.Plot()
}


type Map struct{
	Values [][]float64
	Xlabel, Ylabel string
	Options map[string]string
	Flags   map[string]bool
	Settings []string
	FNAME string
}


func (self *Map) ShiftX(xshift int){
	// === shift X axis of a plot map up by an integer value
	var newVals [][]float64
	
	// --- create an array of the zeros at the start
	var xzeros []float64
	for i := 0; i < xshift; i++{
		xzeros = append(xzeros, 0.0)
	}
	
	for _, row := range(self.Values){
		if row == nil{}
		newRow := make( []float64, len(xzeros) )
		copy(newRow, xzeros)

		for _, val := range(row){
			newRow = append(newRow, val)
		}
		newVals = append(newVals, newRow)
	}
	self.Values = newVals
	xmax := len(self.Values[0]) - len(xzeros)
	self.Settings = append(self.Settings, 
		fmt.Sprintf("set xrange [%v:%v]", xshift, xmax) )
}


func (self *Map) ShiftY(yshift int){
	// === shift Y axis of a plot map up by an integer value
	var newVals [][]float64
	
	// --- determine the number of colums in the matrix
	if len(self.Values) == 0{
		return
	}
	// --- else, there are rows of data
	rowlen := len(self.Values[0])

	// --- create an array of the zeros at the start
	var xzeros []float64
	for i := 0; i < rowlen; i++{
		xzeros = append(xzeros, 0.0)
	}
	
	// --- populate newVals with the appropriate number of blank rows
	for i := 0; i < yshift; i++ {
		newVals = append(newVals, xzeros)
	}

	// --- now append the rows which contain data
	for _, row := range(self.Values){
		newVals = append(newVals, row)
	}

	// --- update self.Values
	self.Values = newVals
	ymax := len(self.Values) - yshift
	self.Settings = append(self.Settings, 
		fmt.Sprintf("set yrange [%v:%v]", yshift, ymax) )
}


func (self *Map) Set(opType, opVal string){
	// === set a single option pair for the plot
	self.Options[opType] = opVal
	switch opType{
		case "title":
			// --- set x-label
			self.Settings = append(self.Settings,
				fmt.Sprintf("set title \"%v\"", opVal) )
		case "xlabel":
			// --- set x-label
			self.Settings = append(self.Settings,
				fmt.Sprintf("set xlabel \"%v\"", opVal) )
		case "ylabel":
			// --- set y-label
			self.Settings = append(self.Settings,
				fmt.Sprintf("set ylabel \"%v\"", opVal) )
		case "xrange":
			// --- set range of the x-axis
			self.Settings = append(self.Settings, 
				fmt.Sprintf("set xrange %v", opVal) )
		case "yrange":
			// --- set range of the x-axis
			self.Settings = append(self.Settings, 
				fmt.Sprintf("set yrange %v", opVal) )
		case "xtics":
			// --- set tics of x-axis
			self.Settings = append(self.Settings, 
				fmt.Sprintf("set xtics %v", opVal) )
	}
}


func (self *Map) SetFlag(flag string){
	// === set a single option flag for the plot
	self.Flags[flag] = true
	switch flag{
		case "log x":
			// --- set x-axis to logscale
		case "log y":
			// --- set y-axis to logscale
	}
}


func (self *Map) Plot(){
	// === plot this scatter chart
	
	// --- create plot data file
	var DATA []string
	DATAname := self.FNAME + ".dat"

	for _, Row := range(self.Values){
		var line string
		for _, Val := range(Row){
			//Val := Column[j]
			line = fmt.Sprintf("%v %v", line, Val)
		}
		DATA = append(DATA, line)
	}

	DATAstr := strings.Join(DATA, "\n")
	// --- write gnuplot data file
	ioutil.WriteFile(DATAname, []byte(DATAstr), 0644)

	// --- construct and write gnuplot input file
	GPname  := self.FNAME + ".inp"
	IMGname := self.FNAME + ".png"

	GPinp := []string{"set terminal png transparent nocrop enhanced size 1024,768",
		fmt.Sprintf("set output \"%v\"", IMGname),
		"set nokey ",
		"set grid"}

	// --- set logscales
	if self.Flags["log x"] && self.Flags["log y"]{
		// --- set both axes to logscale
		GPinp = append(GPinp, "set log xy")
	} else if self.Flags["log x"]{
		// --- set x-axis to logscale
		GPinp = append(GPinp, "set log x")
	} else if self.Flags["log y"]{
		// --- set x-axis to logscale
		GPinp = append(GPinp, "set log y")
	}

	// --- append settings lines
	for _, line := range(self.Settings){
		GPinp = append(GPinp, line)
	}


	// --- smaller plot, add lines
	GPinp = append(GPinp, "set pm3d map")
	GPinp = append(GPinp, 
		fmt.Sprintf("splot \"%s\" matrix", DATAname))

	// --- write plot input to file
	GPstr := strings.Join(GPinp, "\n")
	ioutil.WriteFile(GPname, []byte(GPstr), 0644)
	
	// --- execute gnuplot on file
	gpcmd := exec.Command(GPBIN, GPname)
	err := gpcmd.Run()
	if err != nil {
		panic(err)
	}

	// --- conditionally remove input files
	if !self.Flags["saveinput"]{
		// --- remove the input files
		os.Remove(DATAname)
		os.Remove(GPname)
	}

}


func NewMap(values [][]float64) (plot *Map){
	// === Initialize a new map plot
	plot = new(Map)
	plot.Values  = values
	plot.Options = make(map[string]string)
	plot.Flags   = make(map[string]bool)
	plot.FNAME = "unnamedMap"
	return plot
}