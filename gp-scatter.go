/*
Copyright (c) 2012, Michael Leimon
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL LEIMON BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// --- `gp-map.go` - scatter plot functions

package gnuplot

import (
	"fmt"
	"io/ioutil"
	"strings"
	"os/exec"
	"os"
)


type Scatter struct{
	X, Y []float64
	Xlabel, Ylabel string
	Options map[string]string
	Flags   map[string]bool
	Settings []string
	FNAME string
}


func (self *Scatter) Set(opType, opVal string){
	// === set a single option pair for the plot
	self.Options[opType] = opVal
	switch opType{
		case "title":
			// --- set x-label
			self.Settings = append(self.Settings,
				fmt.Sprintf("set title \"%v\"", opVal) )
		case "xlabel":
			// --- set x-label
			self.Settings = append(self.Settings,
				fmt.Sprintf("set xlabel \"%v\"", opVal) )
		case "ylabel":
			// --- set y-label
			self.Settings = append(self.Settings,
				fmt.Sprintf("set ylabel \"%v\"", opVal) )
		case "xrange":
			// --- set range of the x-axis
			self.Settings = append(self.Settings, 
				fmt.Sprintf("set xrange %v", opVal) )
		case "yrange":
			// --- set range of the x-axis
			self.Settings = append(self.Settings, 
				fmt.Sprintf("set yrange %v", opVal) )
		case "xtics":
			// --- set tics of x-axis
			self.Settings = append(self.Settings, 
				fmt.Sprintf("set xtics %v", opVal) )
	}
}


func (self *Scatter) SetFlag(flag string){
	// === set a single option flag for the plot
	self.Flags[flag] = true
	switch flag{
		case "log x":
			// --- set x-axis to logscale
		case "log y":
			// --- set y-axis to logscale
	}
}


func (self *Scatter) Plot(){
	// === plot this scatter chart
	
	// --- create plot data file
	var DATA []string
	DATAname := self.FNAME + ".dat"

	for i, Xval := range(self.X){
		Yval := self.Y[i]
		DATA = append(DATA, fmt.Sprintf("%v %v", Xval, Yval))
	}

	DATAstr := strings.Join(DATA, "\n")
	// --- write gnuplot data file
	ioutil.WriteFile(DATAname, []byte(DATAstr), 0644)

	// --- construct and write gnuplot input file
	GPname  := self.FNAME + ".inp"
	IMGname := self.FNAME + ".png"

	GPinp := []string{"set terminal png transparent nocrop enhanced size 1024,768",
		fmt.Sprintf("set output \"%v\"", IMGname),
		"set nokey ",
		"set grid"}

	// --- set logscales
	if self.Flags["log x"] && self.Flags["log y"]{
		// --- set both axes to logscale
		GPinp = append(GPinp, "set log xy")
	} else if self.Flags["log x"]{
		// --- set x-axis to logscale
		GPinp = append(GPinp, "set log x")
	} else if self.Flags["log y"]{
		// --- set x-axis to logscale
		GPinp = append(GPinp, "set log y")
	}

	// --- append settings lines
	for _, line := range(self.Settings){
		GPinp = append(GPinp, line)
	}


	// --- smaller plot, add lines
	GPinp = append(GPinp, 
		fmt.Sprintf("plot \"%s\" using 1:2 with linespoints", DATAname))

	GPstr := strings.Join(GPinp, "\n")
	ioutil.WriteFile(GPname, []byte(GPstr), 0644)
	
	//gpcmd := exec.Command(GPexecLoc, GPname)
	gpcmd := exec.Command(GPBIN, GPname)
	err := gpcmd.Run()
	if err != nil {
		panic(err)
	}

	// --- conditionally remove input files
	if !self.Flags["saveinput"]{
		// --- remove the input files
		os.Remove(DATAname)
		os.Remove(GPname)
	}


}


func NewScatter(X, Y []float64) (plot *Scatter){
	// === Initialize a new scatter plot
	plot = new(Scatter)
	plot.Options = make(map[string]string)
	plot.Flags   = make(map[string]bool)
	plot.X = X
	plot.Y = Y
	plot.FNAME = "unnamedScatter"
	return plot
}